#!/usr/bin/env bash
# This is a small script for moving images interactively around using nsxiv.

set -eo pipefail

# Check that dependencies are installed
if ! [ -x "$(command -v nsxiv)" ]; then
  echo "Error: nsxiv is not installed" >&2
  exit 1
fi

if ! [ -d "$1" ] || ! [ -d "$2" ]; then
  echo "usage: $0 <source directory> <destination directory>"
  exit 1
fi

nsxiv --quiet --stdout --thumbnail --animate --recursive --null "$1" \
  | xargs --null mv --interactive --target-directory="$2"
