#!/usr/bin/env bash
# USAGE
# See `./postgres --help`
#
# DESCRIPTION
# This is a utility script for managing a transient postgres container.
# The container is meant to be used in development and testing environments
# where data does not need to be persisted.
#
# CONFIGURATION
# You can configure the container through these environment variables:
#   POSTGRES_USER
#   POSTGRES_PASSWORD
#   POSTGRES_DB
#   POSTGRES_PORT
#   POSTGRES_IMAGE           - URL to container image you want to run
#   POSTGRES_CONTAINER_NAME
#
# DEPENDENCIES
#   docker or podman (aliased to docker)
#   psql

set -eo pipefail

# Defaults
DB_USER="${POSTGRES_USER:=postgres}"
DB_PASSWORD="${POSTGRES_PASSWORD:=password}"
DB_NAME="${POSTGRES_DB:=postgres}"
DB_PORT="${POSTGRES_PORT:=5432}"
POSTGRES_IMAGE="${POSTGRES_IMAGE:=postgres:14-alpine}"
POSTGRES_CONTAINER_NAME="${POSTGRES_CONTAINER_NAME:=myapp-postgres}"

# Check that dependencies are installed
if ! [ -x "$(command -v psql)" ]; then
  echo "Error: psql is not installed" >&2
  exit 1
fi

if ! [ -x "$(command -v docker)" ]; then
  echo "Error: docker is not installed" >&2
  exit 1
fi


ProgName=$(basename "$0")
subcommand=$1

sub_help() {
  echo "Usage: $ProgName <subcommand>"
  echo "Subcommands: "
  echo "  start   start the container"
  echo "  rm      kill and remove the container"
}

sub_start() {
  # if a postgres container already exists with the same name, print
  # instructions to remove it
  EXISTING_CONTAINER=$(docker ps --all --filter \
    "name=$POSTGRES_CONTAINER_NAME" --format '{{.ID}}')
  if [[ -n $EXISTING_CONTAINER ]]; then
    echo "there is a postgres container already running, kill and remove it with" >&2
    echo "    $ProgName rm" >&2
    exit 1
  fi
  
  docker run \
    -e POSTGRES_USER=${DB_USER} \
    -e POSTGRES_PASSWORD=${DB_PASSWORD} \
    -e POSTGRES_DB=${DB_NAME} \
    -p "${DB_PORT}":5432 \
    --name "$POSTGRES_CONTAINER_NAME" \
    -d "$POSTGRES_IMAGE" \
    postgres -N 1000
    
    until PGPASSWORD="${DB_PASSWORD}" psql -h "localhost" -U "${DB_USER}" \
      -p "${DB_PORT}" -d "postgres" -c '\q'; do
      echo "Postgres is still unavaliable - sleeping" >&2
      sleep 1
    done
    
    echo "Postgres is up and running on port ${DB_PORT}!" >&2
}

sub_rm() {
  # kill the container if it is runnning
  RUNNING_CONTAINER=$(docker ps --filter \
    "name=$POSTGRES_CONTAINER_NAME" --format '{{.ID}}')
  if [[ -n $RUNNING_CONTAINER ]]; then
    docker kill "$RUNNING_CONTAINER"
  fi

  docker rm "$POSTGRES_CONTAINER_NAME"
  echo "Removed container '$POSTGRES_CONTAINER_NAME' successfully"
}

case $subcommand in
  "" | "-h" | "--help")
    sub_help
    ;;
  *)
    shift
    sub_"$subcommand" "$@"
    if [ $? = 127 ]; then
      echo "Error: '$subcommand' is not a known subcommand." >&2
      echo "   Run '$ProgName --help' for a list of known subcommands" >&2
      exit 1
    fi
esac
